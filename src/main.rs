use getting_started::index;

use actix_web::{middleware, web, App, HttpServer};

fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "actix_web=info");
    env_logger::init();

    HttpServer::new(|| {
        App::new()
            // enable logger
            .wrap(middleware::Logger::default())
            .default_service(web::resource("").route(web::get().to(index)))
    })
    .bind("127.0.0.1:8080")?
    .run()
}
