use actix_web::{HttpRequest, HttpResponse};
use serde_json::json;
use sys_info::hostname;

pub fn index(req: HttpRequest) -> HttpResponse {
    let path = req.path();
    let h = hostname().unwrap_or_else(|_| String::from("Unknown"));

    let json = json!({
        "hostname": h,
        "path": path
    });

    match serde_json::to_string(&json) {
        Ok(res) => HttpResponse::Ok()
            .content_type("application/json")
            .body(res),
        Err(_) => {
            println!("Hey");
            HttpResponse::NotFound().body("Error")
        }
    }
}
