FROM rust:slim as builder
WORKDIR /usr/src/app
# Create blank project
RUN USER=root cargo init
# Copy Cargo.toml to get dependencies
COPY Cargo.toml .
COPY src src
RUN apt-get update && \
    apt-get install -y musl-tools && \
    rustup target add x86_64-unknown-linux-musl && \
    cargo build --target x86_64-unknown-linux-musl --release && \
    strip target/x86_64-unknown-linux-musl/release/getting_started


FROM scratch
# Copy bin from builder to this new image
COPY --from=builder /usr/src/app/target/x86_64-unknown-linux-musl/release/getting_started /server
# Default command, run app
CMD ["/server"]
