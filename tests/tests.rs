use getting_started::index;

use actix_web::{http, test};
use http::StatusCode;

#[test]
fn test_index() {
    let req = test::TestRequest::with_header("content-type", "text/plain").to_http_request();

    let resp = test::block_on(index(req)).unwrap();
    assert_eq!(resp.status(), StatusCode::OK);
}
