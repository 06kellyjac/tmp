# Working with Rust

## First you need Rust

The widely recommended way to get rust is through the official version manager `rustup`.

You can install it from <https://rustup.rs/> with `curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh` or from your package manager.

## Adding the RLS

The Rust Language Server will be your best friend when it comes to writing code with Rust.
It's very similar in design to other Language Servers like the one for Node and as a result many Editors and IDEs have plugins.

To install the RLS just run `rustup component add rls rust-analysis rust-src`.

<https://areweideyet.com/>

* Atom:
  * <https://atom.io/packages/language-rust>
  * <https://atom.io/packages/ide-rust>
* Emacs:
  * <https://github.com/emacs-lsp/lsp-mode>
  * <https://github.com/brotzeit/rustic>
* VIM:
  * <https://github.com/rust-lang/rust.vim>
  * <https://github.com/racer-rust/vim-racer>
* VSCode:
  * <https://marketplace.visualstudio.com/items?itemName=rust-lang.rust>
    * <https://github.com/rust-lang/rls-vscode>

## Creating a project

To create a new Rust project it's as simple as `cargo init` for a binary project or `cargo init --lib` for a library.
They're fundamentally very similar, the only difference is you'll start with either `main.rs` or `lib.rs` and the library project will have the `Cargo.lock` file in it's `.gitignore`.

## Separating code for readability and testing

Why split binary to main and lib:

<https://rust-lang-nursery.github.io/cli-wg/tutorial/testing.html>
<https://github.com/rust-lang-nursery/api-guidelines/issues/167>
<https://doc.rust-lang.org/stable/book/ch12-03-improving-error-handling-and-modularity.html>

## Non code related improvements

### .editorconfig

### rust-clippy

`rustup component add clippy`

`cargo clippy`

VSCode you need to opt-in to clippy per file or turn it on globally.

### rustfmt

`rustup component add rustfmt`

`cargo fmt`

<https://github.com/rust-lang/rustfmt/blob/master/Configurations.md>
